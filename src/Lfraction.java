import org.junit.runners.model.InitializationError;

import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here

      Lfraction lf1 = new Lfraction(-2, 3);
      if (lf1.pow(1).equals(new Lfraction(2, 3))) {
         System.out.println("1 : correct");
      }
      if (lf1.pow(0).equals(new Lfraction(1, 1))) {
         System.out.println("0 : correct");
      }
      if (lf1.pow(-1).equals(new Lfraction(3, 2))) {
         System.out.println("-1 correct");
      }
      if (lf1.pow(5).equals(new Lfraction(32, 243))) {
         System.out.println("5 correct");
      }
      if (lf1.pow(-2).equals(new Lfraction(9, 4))) {
         System.out.println("-2 correct");
      }

   }

   // TODO!!! instance variables here
   private long numerator;
   private long denominator;
   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      // TODO!!!
      if (b <= 0) {
         throw new IllegalArgumentException("denominator must be bigger than zero");
      }
      long divisor = this.getGCD(a, b);
      numerator = a/divisor;
      denominator = b/divisor;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator; // TODO!!!
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return String.valueOf(this.numerator) + "/" + String.valueOf(this.denominator); // TODO!!!
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m instanceof Lfraction) {
         return ((Lfraction)m).numerator == this.numerator && ((Lfraction)m).denominator == this.denominator;
      }
      return false; // TODO!!!
   }

   /** Hashcode has to be the same for equal fractions and in general, different
    * for different fractions.
    * @return hashcode
    * referring to https://stackoverflow.com/questions/4045063/how-should-i-map-long-to-int-in-hashcode/4045101#4045101
    */
   @Override
   public int hashCode() {
      return (int)(this.numerator>>>32)^(int)this.denominator; // TODO!!!
   }

   /** Find the greatest common divisor of two integers using Euclid algorithm
    * @param a numerator > 0
    * @param b denominator > 0
    * @return gcd long
    * https://en.wikipedia.org/wiki/Greatest_common_divisor
    * https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm
    */
   private static long getGCD(long a, long b) {
      a = Math.abs(a);
      b = Math.abs(b);
      if (a == b) {
         return a;
      }
      if (b == 0) {
         return a;
      } else if (a == 0) {
         return b;
      }

      if (a > b) {
         return getGCD(b, a % b);
      }
      return getGCD(a, b % a);
   }

   /** Power of fractions.
    * @parm int n
    * @return Lfraction m ** n
    */
   public Lfraction pow (int n) {
      if (n == 0) {
         return new Lfraction(1,1);
      } else if (n == 1) {
         return new Lfraction(this.numerator, this.denominator);
      } else if (n == -1) {
         return this.inverse();
      } else if (n > 1) {
         return this.times(this.pow(n - 1));
      }
      return this.pow(-n).inverse();
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long divisor = this.getGCD(this.denominator, m.denominator);
      long denominator = (m.denominator/divisor) * this.denominator;
      long numerator = (m.denominator/divisor) * this.numerator +
              (this.denominator/divisor) * m.numerator;
      return new Lfraction(numerator, denominator); // TODO!!!
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long divisor1 = this.getGCD(m.numerator, this.denominator);
      long divisor2 = this.getGCD(this.numerator, m.denominator);
      long numerator = (this.numerator/divisor2)*(m.numerator/divisor1);
      long denominator = (this.denominator/divisor1)*(m.denominator/divisor2);
      return new Lfraction(numerator, denominator); // TODO!!!
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.numerator == 0) throw new IllegalArgumentException("division by zero exception.");

      if (this.numerator < 0) return new Lfraction(-this.denominator, -this.numerator);
      return new Lfraction(this.denominator, this.numerator); // TODO!!!
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator); // TODO!!!
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite()); // TODO!!!
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0) throw new IllegalArgumentException("division by zero exception.");
      long n = this.numerator * m.inverse().numerator;
      long d = this.denominator * m.inverse().denominator;
      return new Lfraction(n, d); // TODO!!!
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.equals(m)) return 0;

      if (this.toDouble() > m.toDouble()) {
         return 1;
      }
      return -1; // TODO!!!
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator); // TODO!!!
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long)(this.numerator/this.denominator); // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(this.numerator % this.denominator, this.denominator); // TODO!!!
   }

   /** Approximate value of the fraction.
    * @return real value of this fraction
    */
   public double toDouble() {
      return (double) this.numerator/this.denominator; // TODO!!!
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d <= 0) throw new IllegalArgumentException("value of d must be bigger than zero");
      if (f == 0) return new Lfraction(0, d);
      long i = 1;
      double prev = 0;
      double value = 1./d;
      while (value < Math.abs(f)) {
         i++;
         prev = value;
         value = (double) i / (double) d;
      }


      if (f < 0) {
         i *= -1;
         f *= -1;
      }

      if (Math.abs(value - f) > Math.abs(prev - f))  {
         return new Lfraction(i-1, d);
      }

      return new Lfraction(i, d); // TODO!!!
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] strArray = s.split("/", 2);
      if (strArray.length != 2) throw new IllegalArgumentException("given string: " + s +" cannot be converted into Lfraction format.");
      String regex = "-?[0-9]+";
      for (String aa: strArray) {
         System.out.println(aa);
      }
      if (!strArray[0].matches(regex) || !strArray[1].matches(regex)) {
         throw new IllegalArgumentException("given string: " + s + " contains illegal characters.");
      }
      long numerator = Long.parseLong(strArray[0]);
      long denominator = Long.parseLong(strArray[1]);
      return new Lfraction(numerator, denominator); // TODO!!!
   }
}

